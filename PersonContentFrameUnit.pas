unit PersonContentFrameUnit;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AbstractContentFrameUnit,
	Vcl.StdCtrls, Vcl.ExtCtrls;

type
	TPersonContentFrame = class( TAbstractContentFrame )
		Label1 : TLabel;
		Edit1 : TEdit;
	protected
		// These Do-functions are called by their respective Ok/Cancel methods, which are in
		// turn called by the generic dialog when the user clicks on a dialog button. This
		// illustrates the interfacing between the generic dialog and the form.
		procedure DoOk; override;
		procedure DoCancel; override;

	public
	end;

implementation

{$R *.dfm}

procedure TPersonContentFrame.DoOk;
begin

	inherited;

	ShowMessage( 'The person dialog was OK''d' );

end;

procedure TPersonContentFrame.DoCancel;
begin

	inherited;

	ShowMessage( 'The person dialog was cancelled' );

end;

end.
