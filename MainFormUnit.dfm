object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'The Fame Inheritance Example'
  ClientHeight = 378
  ClientWidth = 662
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object OpenDialogButton1: TButton
    Left = 32
    Top = 40
    Width = 121
    Height = 25
    Caption = 'Open Person Dialog'
    TabOrder = 0
    OnClick = OpenDialogButton1Click
  end
  object OpenDialogButton2: TButton
    Left = 32
    Top = 71
    Width = 121
    Height = 25
    Caption = 'Open Company Dialog'
    TabOrder = 1
    OnClick = OpenDialogButton2Click
  end
end
