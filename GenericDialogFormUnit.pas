unit GenericDialogFormUnit;

(*

	The generic dialog form gets created only once as the application starts up, making for
	a very lean startup. The generic form could even be created on demand, using a simple getter
	function that creates the form as it is needed.

*)

interface

uses
	AbstractContentFrameUnit,

	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
	TGenericDialogForm = class( TForm )
		PanelContent : TPanel;
		PanelButtons : TPanel;
		OkButton : TButton;
		CancelButton : TButton;
		procedure CancelButtonClick( Sender : TObject );
		procedure OkButtonClick( Sender : TObject );

	private
		FContentFrame : TAbstractContentFrame;
		FContentFrameType : TContentFrameType;

		procedure SetContentFrameType( const AContentFrameType : TContentFrameType );

		procedure DoOkClick;
		procedure DoCancelClick;

	public
		property ContentFrame : TAbstractContentFrame read FContentFrame;
		property ContentFrameType : TContentFrameType read FContentFrameType write SetContentFrameType;

	end;

var
	GenericDialogForm : TGenericDialogForm;

implementation

{$R *.dfm}

procedure TGenericDialogForm.SetContentFrameType( const AContentFrameType : TContentFrameType );
begin

	// Discard a possible earlier frame content. This could be done by other means, of course
	FreeAndNil( FContentFrame );

	// Set the new frame type if you need to
	FContentFrameType := AContentFrameType;

	// Create the new content frame and assign it as the new ContentFrame
	FContentFrame := AContentFrameType.Create( Application.MainForm );
	FContentFrame.Parent := PanelContent;

end;

procedure TGenericDialogForm.DoOkClick;
begin

	Assert( Assigned( FContentFrame ), 'Content frame not assigned' );

	// This will call the Ok method of the current form. Notice that we don't need
	// to include the specialized frame class in the uses statement of this unit, only
	// its abstract parent.
	FContentFrame.Ok;

end;

procedure TGenericDialogForm.DoCancelClick;
begin

	Assert( Assigned( FContentFrame ), 'Content frame not assigned' );

	// This will call the Cancel mathod of the current form. Notice that we don't need
	// to include the specialized frame class in the uses statement of this unit, only
	// its abstract parent.
	FContentFrame.Cancel;

end;

procedure TGenericDialogForm.OkButtonClick( Sender : TObject );
begin

	// Nothing but executing the private Do-methods in an OnClick event
	DoOkClick;

end;

procedure TGenericDialogForm.CancelButtonClick( Sender : TObject );
begin

	// Nothing but executing the private Do-methods in an OnClick event
	DoCancelClick;

end;

end.
