unit CompanyContentFrameUnit;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AbstractContentFrameUnit,
	Vcl.StdCtrls, Vcl.ExtCtrls;

type
	TCompanyContentFrame = class( TAbstractContentFrame )
		Edit1 : TEdit;
		Label1 : TLabel;
	protected
		// These Do-functions are called by their respective Ok/Cancel methods, which are in
		// turn called by the generic dialog when the user clicks on a dialog button. This
		// illustrates the interfacing between the generic dialog and the form.
		procedure DoOk; override;
		procedure DoCancel; override;

	end;

var
	CompanyContentFrame : TCompanyContentFrame;

implementation

{$R *.dfm}

procedure TCompanyContentFrame.DoOk;
begin

	inherited;

	ShowMessage( 'The company dialog was OK''d' );

end;

procedure TCompanyContentFrame.DoCancel;
begin

	inherited;

	ShowMessage( 'The company dialog was cancelled' );

end;

end.
