object GenericDialogForm: TGenericDialogForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'The Dialog Frame'
  ClientHeight = 388
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PanelContent: TPanel
    Left = 0
    Top = 0
    Width = 672
    Height = 328
    Align = alClient
    Caption = 'PanelContent'
    ShowCaption = False
    TabOrder = 0
  end
  object PanelButtons: TPanel
    Left = 0
    Top = 328
    Width = 672
    Height = 60
    Align = alBottom
    Caption = 'PanelButtons'
    ShowCaption = False
    TabOrder = 1
    object OkButton: TButton
      Left = 472
      Top = 16
      Width = 75
      Height = 25
      Caption = 'OkButton'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = OkButtonClick
    end
    object CancelButton: TButton
      Left = 568
      Top = 16
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'CancelButton'
      ModalResult = 2
      TabOrder = 1
      OnClick = CancelButtonClick
    end
  end
end
