unit AbstractContentFrameUnit;

(*
	The Abstract Content Frame introduces basic functionality like Ok, Cancel,
	positioning, a title panel etc. When the OK or Cancel button of the
	generic dialog form is clicked, it calls the methods OK or Cancel which
	in turn calls the virtual methods DoOk or DoCancel. These can be overridden
	in the descandant frames to perform specific functionality for the
	respective frame/content type.
*)

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
	Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls;

type
	TAbstractContentFrame = class( TFrame )
		CaptionPanel : TPanel;

	protected
		procedure DoOk; virtual;
		procedure DoCancel; virtual;

	public
		// Notice that the Ok(Cancel method (called from outside of this frame class) and the
		// DoOk and DoCancel virtual methods are separated. This makes for a cleaner implementation
		// in the descendants, and is in line with the pattern to never, ever have functionality
		// code in Delphi form event methods (like OnClick).
		// In my oppinion, no funktional code should ever be put in public methods. Public
		// methods should always use private methods or, if they should be virtual, protected methods.
		procedure Ok;
		procedure Cancel;

	end;

	TContentFrameType = class of TAbstractContentFrame;

implementation

{$R *.dfm}

procedure TAbstractContentFrame.DoOk;
begin

end;

procedure TAbstractContentFrame.DoCancel;
begin

end;

procedure TAbstractContentFrame.Ok;
begin

	DoOk;

end;

procedure TAbstractContentFrame.Cancel;
begin

	DoCancel;

end;

end.
