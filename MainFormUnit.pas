unit MainFormUnit;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
	TMainForm = class( TForm )
		OpenDialogButton1 : TButton;
		OpenDialogButton2 : TButton;
		procedure OpenDialogButton1Click( Sender : TObject );
		procedure OpenDialogButton2Click( Sender : TObject );
	end;

var
	MainForm : TMainForm;

implementation

{$R *.dfm}

uses
	GenericDialogFormUnit,
	PersonContentFrameUnit,
	CompanyContentFrameUnit;

procedure TMainForm.OpenDialogButton1Click( Sender : TObject );
begin

	(*
		Getting the correct frame could very easily be changed to use a frame factory pattern
	*)

	GenericDialogForm.ContentFrameType := TPersonContentFrame;
	GenericDialogForm.ShowModal;

end;

procedure TMainForm.OpenDialogButton2Click( Sender : TObject );
begin

	(*
		Getting the correct frame could very easily be changed to use a frame factory pattern
	*)

	GenericDialogForm.ContentFrameType := TCompanyContentFrame;
	GenericDialogForm.ShowModal;

end;

end.
