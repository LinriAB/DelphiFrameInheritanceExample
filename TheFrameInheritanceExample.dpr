program TheFrameInheritanceExample;

uses
	Vcl.Forms,
	MainFormUnit in 'MainFormUnit.pas' {MainForm} ,
	AbstractContentFrameUnit in 'AbstractContentFrameUnit.pas' {AbstractContentFrame: TFrame} ,
	GenericDialogFormUnit in 'GenericDialogFormUnit.pas' {GenericDialogForm} ,
	PersonContentFrameUnit in 'PersonContentFrameUnit.pas' {PersonContentFrame: TFrame} ,
	CompanyContentFrameUnit in 'CompanyContentFrameUnit.pas' {CompanyContentFrame: TFrame};

{$R *.res}

begin
	Application.Initialize;
	Application.MainFormOnTaskbar := True;
	Application.CreateForm( TMainForm, MainForm );
	Application.CreateForm( TGenericDialogForm, GenericDialogForm );
	Application.Run;

end.
